const tokenUrl = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/50975eaf-e3c7-4f5a-a3fd-1a2233accab2/token";
const instanceLocator = "v1:us1:50975eaf-e3c7-4f5a-a3fd-1a2233accab2";

export { tokenUrl, instanceLocator }