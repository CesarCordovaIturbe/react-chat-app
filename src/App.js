import React from 'react';
import MessageList from './components/MessageList.js'
import SendMessageForm from './components/SendMessageForm'
import RoomList from './components/RoomList'
import NewRoomForm from './components/NewRoomForm'
import { tokenUrl, instanceLocator } from './config.js';


class App extends React.Component {

  constructor(){
    super() 
      this.state = {
        roomId: null,
        messages: [],
        joinableRooms: [],
        joinedRoom: []
      }
      this.sendMessage = this.sendMessage.bind(this)
      this.subscribeToRoom = this.subscribeToRoom.bind(this)
      this.getRooms = this.getRooms.bind(this)
      this.createRoom = this.createRoom.bind(this)
  }

    componentDidMount() {
      const chatManager = new window.Chatkit.ChatManager({
        instanceLocator,
        userId: "test",
        tokenProvider: new window.Chatkit.TokenProvider({
          url: tokenUrl
        })
      })

      chatManager.connect()
      .then(currentUser => {
        this.currentUser = currentUser
        this.getRooms()
      })
      .catch(err => console.log ('error on connecting: ', err ))
    }
    

    getRooms() {
      this.currentUser.getJoinableRooms()
        .then(joinableRooms => {
          this.setState ({
            joinableRooms,
            joinedRoom: this.currentUser.rooms
          })
        })
        .catch(err => console.log ('error on joinableRooms: ', err ))
    }

    subscribeToRoom(roomId) {
      this.setState ({  messages: [] , roomId: roomId})
      this.currentUser.subscribeToRoom({
        roomId: roomId,
        hooks: {
          onMessage: message => {
            this.setState({
              messages: [...this.state.messages, message]
            })
          }
        }
      })
      .then ( room => {
        this.getRooms()
      })
      .catch(err => console.log ('error on subscribing to room: ', err))
      console.log(roomId)
    }


    sendMessage(text) {
      this.currentUser.sendMessage({
        text,
        roomId: this.state.roomId
      })
    }

    createRoom(name) {
      this.currentUser.createRoom({
        name
      })
      .then(room => this.subscribeToRoom(room.id))
      .catch(err => console.log('error with createRoom', err))
    }

  render() {
      return (
          <div className="app">
              <RoomList 
                roomId={this.state.roomId}
                subscribeToRoom={this.subscribeToRoom} 
                rooms={[...this.state.joinableRooms, ...this.state.joinedRoom]}/>
              <MessageList 
                roomId={this.state.roomId}
                messages={this.state.messages} />
              <SendMessageForm  
                disabled={!this.state.roomId}
                sendMessage={this.sendMessage} />
              <NewRoomForm createRoom={this.createRoom}/>
          </div>
      );
  }
}


export default App